%undefine __cmake_in_source_build

%global test_sha ff3133cdb6cb496ee1d2c3231bfa35006a5e8410
%global qt6 1

Name:    poppler
Version: 24.03.0
Release: 6
Summary: PDF rendering library
License: (GPL-2.0-only OR GPL-3.0-only) AND GPL-2.0-or-later AND LGPL-2.0-or-later AND LGPL-2.1-or-later AND MIT
URL:     https://poppler.freedesktop.org/
Source0: https://poppler.freedesktop.org/poppler-%{version}.tar.xz
Source1: https://gitlab.freedesktop.org/poppler/test/-/archive/%{test_sha}/test-%{test_sha}.tar.bz2

Patch1:  poppler-0.90.0-position-independent-code.patch
Patch3:  poppler-21.01.0-glib-introspection.patch

Patch6000:	backport-CVE-2024-6239.patch
Patch6001:	backport-CVE-2024-56378.patch

BuildRequires: make
BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: gettext-devel
BuildRequires: pkgconfig(cairo)
BuildRequires: pkgconfig(cairo-ft)
BuildRequires: pkgconfig(cairo-pdf)
BuildRequires: pkgconfig(cairo-ps)
BuildRequires: pkgconfig(cairo-svg)
BuildRequires: pkgconfig(fontconfig)
BuildRequires: pkgconfig(freetype2)
BuildRequires: pkgconfig(gdk-pixbuf-2.0)
BuildRequires: pkgconfig(gio-2.0)
BuildRequires: pkgconfig(gobject-2.0)
BuildRequires: pkgconfig(gobject-introspection-1.0)
BuildRequires: pkgconfig(gtk+-3.0)
BuildRequires: pkgconfig(gtk-doc)
BuildRequires: pkgconfig(lcms2)
BuildRequires: pkgconfig(libjpeg)
BuildRequires: pkgconfig(libopenjp2)
BuildRequires: pkgconfig(libpng)
BuildRequires: pkgconfig(libtiff-4)
BuildRequires: pkgconfig(nss)
BuildRequires: pkgconfig(poppler-data)
BuildRequires: pkgconfig(Qt5Core)
BuildRequires: pkgconfig(Qt5Gui)
BuildRequires: pkgconfig(Qt5Test)
BuildRequires: pkgconfig(Qt5Widgets)
BuildRequires: pkgconfig(Qt5Xml)
%if 0%{?qt6}
BuildRequires: cmake(Qt6Core)
BuildRequires: cmake(Qt6Gui)
BuildRequires: cmake(Qt6Test)
BuildRequires: cmake(Qt6Widgets)
BuildRequires: cmake(Qt6Xml)
%endif
BuildRequires: boost-devel
BuildRequires: gpgme-devel
BuildRequires: cpp-gpgme
BuildRequires: libcurl-devel

Requires: poppler-data
Obsoletes: poppler-glib-demos < 0.60.1-1

%description
%{name} is a PDF rendering library.

%package devel
Summary: Libraries and headers for poppler
Requires: %{name} = %{version}-%{release}

%description devel
You should install the poppler-devel package if you would like to
compile applications based on poppler.

%package glib
Summary: Glib wrapper for poppler
Requires: %{name} = %{version}-%{release}

%description glib
%{summary}.

%package glib-devel
Summary: Development files for glib wrapper
Requires: %{name}-glib = %{version}-%{release}
Requires: %{name}-devel = %{version}-%{release}
Suggests: %{name}-doc = %{version}-%{release}

%description glib-devel
%{summary}.

%package glib-doc
Summary: Documentation for glib wrapper
BuildArch: noarch

%description glib-doc
%{summary}.

%package qt5
Summary: Qt5 wrapper for poppler
Requires: %{name} = %{version}-%{release}
Obsoletes: %{name}-qt < 0.90.0-9
%description qt5
%{summary}.

%package qt5-devel
Summary: Development files for Qt5 wrapper
Requires: %{name}-qt5 = %{version}-%{release}
Requires: %{name}-devel = %{version}-%{release}
Requires: qt5-qtbase-devel
Obsoletes: %{name}-qt-devel < 0.90.0-9
%description qt5-devel
%{summary}.

%if 0%{?qt6}
%package qt6
Summary: Qt6 wrapper for poppler
Requires: %{name} = %{version}-%{release}
%description qt6
%{summary}.

%package qt6-devel
Summary: Development files for Qt6 wrapper
Requires: %{name}-qt6 = %{version}-%{release}
Requires: %{name}-devel = %{version}-%{release}
Requires: qt6-qtbase-devel
%description qt6-devel
%{summary}.
%endif

%package cpp
Summary: Pure C++ wrapper for poppler
Requires: %{name} = %{version}-%{release}

%description cpp
%{summary}.

%package cpp-devel
Summary: Development files for C++ wrapper
Requires: %{name}-cpp = %{version}-%{release}
Requires: %{name}-devel = %{version}-%{release}

%description cpp-devel
%{summary}.

%package utils
Summary: Command line utilities for converting PDF files
Requires: %{name} = %{version}-%{release}
%description utils
Command line tools for manipulating PDF files and converting them to
other formats.

%package_help

%prep
%autosetup -p1
tar xf %{S:1}
chmod -x poppler/CairoFontEngine.cc
# disable test check_signature_basics, it will timeout
sed -i "/check_signature_basics/d" {qt5,qt6}/tests/CMakeLists.txt

%build
%cmake \
  -DENABLE_CMS=lcms2 \
  -DENABLE_DCTDECODER=libjpeg \
  -DENABLE_GTK_DOC=ON \
  -DENABLE_LIBOPENJPEG=openjpeg2 \
  -DENABLE_UNSTABLE_API_ABI_HEADERS=ON \
  -DENABLE_ZLIB=OFF \
  -DTESTDATADIR=%{_builddir}/%{buildsubdir}/test-%{test_sha}
%cmake_build

%install
%cmake_install

%check
%ctest
export PKG_CONFIG_PATH=%{buildroot}%{_datadir}/pkgconfig:%{buildroot}%{_libdir}/pkgconfig
test "$(pkg-config --modversion poppler)" = "%{version}"
test "$(pkg-config --modversion poppler-cpp)" = "%{version}"
test "$(pkg-config --modversion poppler-glib)" = "%{version}"
test "$(pkg-config --modversion poppler-qt5)" = "%{version}"
%if 0%{?qt6}
test "$(pkg-config --modversion poppler-qt6)" = "%{version}"
%endif

%files
%doc README.md
%license COPYING
%{_libdir}/libpoppler.so.135*

%files devel
%{_libdir}/pkgconfig/poppler.pc
%{_libdir}/libpoppler.so
%dir %{_includedir}/poppler/
# xpdf headers
%{_includedir}/poppler/*.h
%{_includedir}/poppler/fofi/
%{_includedir}/poppler/goo/
%{_includedir}/poppler/splash/

%files glib
%{_libdir}/libpoppler-glib.so.8*
%{_libdir}/girepository-1.0/Poppler-0.18.typelib

%files glib-devel
%{_libdir}/pkgconfig/poppler-glib.pc
%{_libdir}/libpoppler-glib.so
%{_datadir}/gir-1.0/Poppler-0.18.gir
%{_includedir}/poppler/glib/

%files glib-doc
%license COPYING
%{_datadir}/gtk-doc/

%files qt5
%{_libdir}/libpoppler-qt5.so.1*

%files qt5-devel
%{_libdir}/libpoppler-qt5.so
%{_libdir}/pkgconfig/poppler-qt5.pc
%{_includedir}/poppler/qt5/

%files cpp
%{_libdir}/libpoppler-cpp.so.0*

%files cpp-devel
%{_libdir}/pkgconfig/poppler-cpp.pc
%{_libdir}/libpoppler-cpp.so
%{_includedir}/poppler/cpp

%files utils
%{_bindir}/pdf*
%{_mandir}/man1/*

%if 0%{?qt6}
%files qt6
%{_libdir}/libpoppler-qt6.so.3*

%files qt6-devel
%{_libdir}/libpoppler-qt6.so
%{_libdir}/pkgconfig/poppler-qt6.pc
%{_includedir}/poppler/qt6/
%endif

%files help
%doc README.md
%{_mandir}/man1/*

%changelog
* Tue Feb 04 2025 Funda Wang <fundawang@yeah.net> - 24.03.0-6
- fix build with latest cmake migration

* Tue Dec 24 2024 lingsheng <lingsheng1@h-partners.com> - 24.03.0-5
- fix CVE-2024-56378

* Tue Dec 17 2024 lingsheng <lingsheng1@h-partners.com> - 24.03.0-4
- fix force out-of-source build
- disable test check_signature_basics

* Thu Nov 21 2024 Funda Wang <fundawang@yeah.net> - 24.03.0-3
- adopt to new cmake macro
- force out-of-source build
- update test tarball

* Mon Jun 24 2024 Zhao Mengmeng <zhaomengmeng@kylinos.cn> - 24.03.0-2
- Type:CVE
- CVE:CVE-2024-6239
- SUG:NA
- DESC:fix CVE-2024-6239

* Tue Apr 02 2024 liweigang <liweiganga@uniontech.com> - 24.03.0-1
- update to version 24.03.0

* Thu Dec 28 2023 Paul Thomas <paulthomas100199@gmail.com> - 23.12.0-1
- update to version 23.12.0

* Tue Aug 08 2023 yajun<yajun@kylinos.cn> - 23.08.0-1
- update to upstream version 23.08.0

* Tue Mar 14 2023 zhangpan <zhangpan103@h-partners.com> - 22.01.0-3
- Type:CVE
- CVE:CVE-2022-27337
- SUG:NA
- DESC:fix CVE-2022-27337

* Tue Sep 06 2022 qz_cx <wangqingzheng@kylinos.cn> - 22.01.0-2
- Type:CVE
- CVE:CVE-2022-38784
- SUG:NA
- DESC: fix CVE-2022-38784
- fix CVE-2022-38784

* Mon Jun 13 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 22.01.0-1
- Update to 22.01.0

* Tue Sep 07 2021 chenchen <chen_aka_jan@163.com> - 0.90.0-2
- add help moudle for ISO creating

* Tue Aug 24 2021 chenchen <chen_aka_jan@163.com> - 0.90.0-1
- update to 0.90.0

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 0.67.0-8
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Sat Jan 23 2021 wangye <wangye70@huawei.com> - 0.67.0-7
- Type:cves
- Id:NA
- SUG:NA
- DESC:fix CVE-2018-16646 CVE-2018-18897 CVE-2018-19060 CVE-2018-20481 CVE-2019-14494 CVE-2019-7310
 
* Thu Oct 29 2020 yanan <yanan@huawei.com> - 0.67.0-6
- Type:cves
- Id:NA
- SUG:NA
- DESC:fix CVE-2019-10872 

* Mon Jan 20 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.67.0-5
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:fix cves

* Mon Jan 20 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.67.0-4
- Type:cve
- Id:NA
- SUG:NA
- DESC:fix cves

* Mon Oct 14 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.67.0-3
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:Adjust sub-package relationship

* Fri Sep 20 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.67.0-2
- Package init
